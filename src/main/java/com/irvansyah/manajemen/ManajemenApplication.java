package com.irvansyah.manajemen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManajemenApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManajemenApplication.class, args);
    }

}
