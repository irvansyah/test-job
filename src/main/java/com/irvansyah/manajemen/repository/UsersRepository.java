package com.irvansyah.manajemen.repository;

import com.irvansyah.manajemen.model.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {

    @Query(value = "select *from users", nativeQuery = true)
    Iterable<Users> findAll();

    Optional<Users> findByEmail(String email);

    Optional<Users> findById(Long id_users);

}
