package com.irvansyah.manajemen.repository;

import com.irvansyah.manajemen.model.Roles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends CrudRepository<Roles, Long> {

    @Query("SELECT u FROM Roles u WHERE u.id_roles = ?1")
    Roles findOneId(Long id);

//    Roles findByName(String name);
}
