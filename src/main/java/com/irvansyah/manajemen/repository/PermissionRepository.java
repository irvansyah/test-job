package com.irvansyah.manajemen.repository;

import com.irvansyah.manajemen.model.Permission;
import com.irvansyah.manajemen.model.Roles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long> {

    @Query("SELECT u FROM Permission u WHERE u.name = ?1")
    Permission findOneName(String id);
}
