package com.irvansyah.manajemen.service;

import com.irvansyah.manajemen.exception.ResourceNotFoundException;
import com.irvansyah.manajemen.model.Users;
import com.irvansyah.manajemen.repository.RolesRepository;
import com.irvansyah.manajemen.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RolesRepository rolesRepository;

    public Users createUsers (Users model) {
       return usersRepository.save(model);
    }


        public List<Users> getAllUsers(){
            return (List<Users>) usersRepository.findAll();

        }

    public ResponseEntity<Users> getDataByEmail (String email) {
        Optional<Users> users = usersRepository.findByEmail(email);
         if (users.isPresent()) {
             return new ResponseEntity<>(users.get(), HttpStatus.OK);
         } else {
             return new ResponseEntity<>(HttpStatus.NOT_FOUND);
         }
    }

    public ResponseEntity<Users> getById(Long id_users) {
        Users users = usersRepository.findById(id_users).orElseThrow(()
                -> new ResourceNotFoundException("User not exit with id" +id_users));
        return ResponseEntity.ok(users);

    }

}
