package com.irvansyah.manajemen.service;

import com.irvansyah.manajemen.model.Permission;
import com.irvansyah.manajemen.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PermissionService {

    @Autowired
    PermissionRepository permissionRepository;

    public Permission createPermission(Permission permission){
        return  permissionRepository.save(permission);
    }
}
