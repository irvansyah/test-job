package com.irvansyah.manajemen.service;

import com.irvansyah.manajemen.model.Activity;
import com.irvansyah.manajemen.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ActivityService {

    @Autowired
    private ActivityRepository activityRepository;


    public Activity createActivity (Activity activity) {
        return activityRepository.save(activity);
    }
}
