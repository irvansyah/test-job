package com.irvansyah.manajemen.service;

import com.irvansyah.manajemen.model.Permission;
import com.irvansyah.manajemen.model.Roles;
import com.irvansyah.manajemen.repository.PermissionRepository;
import com.irvansyah.manajemen.repository.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RolesService {

    @Autowired
    private RolesRepository rolesRepostory;
    @Autowired
    private PermissionRepository permissionRepository;
    public Roles CreateRoles(Roles roles){
        return rolesRepostory.save(roles);
    }


    public void  assignPermission(Map<String,Object> body) {
        Long convertedLong = Long.parseLong(body.get("id_roles").toString());
        Roles roles = rolesRepostory.findOneId(convertedLong);
        List<Object> list = new ArrayList<>();
        if (body.get("permission").getClass().isArray()){
            list = Arrays.asList((Object[])body.get("permission"));
        }else if (body.get("permission") instanceof Collection) {
            list = new ArrayList<>((Collection<?>)body.get("permission"));
        }
        Set<Permission> permissions = new HashSet<>();
        list.stream().forEach(value -> {
            Permission permission = permissionRepository.findOneName(value.toString());
            if (permission != null){
                permissions.add(permission);
                System.out.println(permission);
                roles.setPermission(permissions);
            }
        });
        rolesRepostory.save(roles);



    }

    public Iterable<Roles> findAll() {
        return rolesRepostory.findAll();
    }


}

