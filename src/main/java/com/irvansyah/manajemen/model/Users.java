package com.irvansyah.manajemen.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_users;
    private String email;
    private String name;
    private String password;
    private String username;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="id_roles", nullable=false)
    private Roles roles;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    private Set<Transaction> transactions;

    @JsonIgnore
    @OneToMany(mappedBy = "users")
    private Set<Activity> activities;


}