package com.irvansyah.manajemen.model;

public enum Type {

    CREDIT,
    DEBIT
}
