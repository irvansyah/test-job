package com.irvansyah.manajemen.delivery.controller;

import com.irvansyah.manajemen.model.Permission;
import com.irvansyah.manajemen.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionServices;

    @PostMapping("/created")
    private ResponseEntity<?> CreatePermission(@RequestBody Permission permission){
        return ResponseEntity.ok().body(permissionServices.createPermission(permission));
    }
}
