package com.irvansyah.manajemen.delivery.controller;

import com.irvansyah.manajemen.model.Users;
import com.irvansyah.manajemen.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping("/getAllUsers")
    public List<Users> getAllUsers(){
        return usersService.getAllUsers();
    }

    @PostMapping("/create")
    public Users createUsers(@RequestBody Users model) {
        return usersService.createUsers(model);
    }

    @GetMapping("/getUser/{email}")
    public ResponseEntity<Users> getByEmail(@RequestParam String email) {
        return usersService.getDataByEmail(email);
    }

    @GetMapping("/getUser/{id}")
    public ResponseEntity<Users> getById(@RequestParam Long id_users) {
        return usersService.getById(id_users);
    }

}
