package com.irvansyah.manajemen.delivery.controller;

import com.irvansyah.manajemen.model.Roles;
import com.irvansyah.manajemen.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/roles")
public class RolesController {

    @Autowired
    private RolesService rolesServices;

    @PostMapping("/created")
    private ResponseEntity<?> createRoles(@RequestBody Roles roles){
        Roles roles1 = rolesServices.CreateRoles(roles);
        return ResponseEntity.ok().body(roles1);
    }
    @PostMapping("/assign")
    private ResponseEntity<?> AssignPermission(@RequestBody Map<String,Object> roles){
        rolesServices.assignPermission(roles);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/")
    private ResponseEntity<?> getRoles(){
        Iterable<Roles>  roles= rolesServices.findAll();
        return ResponseEntity.ok().body(roles);
    }
}
