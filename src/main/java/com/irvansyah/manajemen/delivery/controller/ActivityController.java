package com.irvansyah.manajemen.delivery.controller;

import com.irvansyah.manajemen.model.Activity;
import com.irvansyah.manajemen.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @PostMapping
    public ResponseEntity<Object> createActivity(@RequestBody Activity activity){
        return ResponseEntity.ok().body(activityService.createActivity(activity));
    }
}
